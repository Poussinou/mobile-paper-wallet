# Mobile Paper Wallet

A paper wallet like mobile application which allows you to create a single use wallet on your phone. Programmed using Python and Kivy.

## Idea
Imagine creating a token wich allows a person to go to a merchant and exchange it for bread or food, 1 bread token = 1 loaf of bread. Upon reciving the token the user will go to the bakery and expose the wif key qrcode. The bakery will scan it, withdraw fund and handle the user a loaf of bread.

User doesn't need to have an active internet connection on their mobile.

## ScreenShots
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.jpg"  width="250" height="542">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg"  width="250" height="542">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.jpg"  width="250" height="542">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.jpg"  width="250" height="542">

### Generating New Keys
As generated walletsare single use wallet, you can generate another one from the Keys screen. 

#### assword for Generating a Wallet
To create a new wallet, user is asked to enter a password so they don't mistakenly hide the older wallet.
Current password is hard coded as `a` (just letter a)

## Supported coins
This wallet was intended to work with the Simple Ledger Protocol but it can works with any coin that uses the CashAddr specification. On demand and incentives, I might add support for other coins.

Tested with:

 * Simple Ledger Protocl (simpleledger)
 * Bitcoin Cash (bitcoincash) - balance query not supported yet

 ## Usage

 To use the app you can send coins of supported types to the address on the address screen either by scanning the QR code or by clicking on the screen and copying the address. 

 To withdraw tokens you use a wallet that supports **sweep** functionality. A wallet that works with Both BCH and SLP is available on https://bchn-wallet.fullstack.cash/

## License
GPL v3 or later
