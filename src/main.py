#!/usr/bin/env python3

# Kivy imports
from kivy.core.window import Window
from kivy.core.clipboard import Clipboard
from kivy.app import App
from kivy.lang import Builder
from kivy.lang import Observable
# ~ from kivy.logger import Logger
from kivy.logger import Logger, LOG_LEVELS

Logger.setLevel(LOG_LEVELS["debug"])
from kivy.properties import (
                            ObjectProperty,
                            StringProperty,
                            BooleanProperty,
                            NumericProperty,
                            ListProperty
                            )

# Kivy uix imports
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.checkbox import CheckBox
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.image import Image
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.popup import Popup
from kivy.uix.progressbar import ProgressBar
from kivy.utils import platform

# Python imports
from functools import partial
from os.path import dirname, join
import threading
import requests
import datetime
import gettext #multilanguage support
import sqlite3 #database to store data
import json
import time
import os

# External libraries imports
from bitcoinaddress import Wallet
from ecashaddress import convert
from ecashaddress.convert import Address
import pyqrcode #Generates QRcodes

# Importing peewee database module
from models import Settings, Wallets, db_save_wallets, db_save_settings

__version__ = "1.3.8"

# Variables

app_name = 'mobile_paper_wallet'
database_file="sqlite3.db"
address_qrcode_file = "media/address.png"
wif_qrcode_file = "media/wif.png"
media_dir = "media/"
default_coin_prefix = "simpleledger"
default_token_id = "926894cbf50269b15c97559b9acfc1bd88cd5f20703313ce0ea0683ecdb40911"
help_link = "https://gitlab.com/uak/mobile-paper-wallet"
default_balance_api = 'https://api.fullstack.cash/v5/psf/slp/'
default_testnet=False
layout_file = 'layout.kv'

custom_font = "data/Amiri-Regular.ttf" #works with Arabic & English letters

# ~ default_balance_api = "https://rest.bch.actorforth.org/v2/slp/balance"
# Password to allow deleting keys

settings = {
    "confirm_delete_text":"delete",
    "app_language":"en",
    "balance_api":"",
    "token_id":"",
    "coin_prefix":"",
    "prefix_changed_recently":False
}

supported_coins = {
    "bitcoin":{"testnet":False},
    "bitcoincash":{"testnet":False},
    "simpleledger":{"testnet":False},
    "ergon":{"testnet":False},
    "nexa":{"testnet":False},
    "bchtest":{"testnet":True},
    "slptest":{"testnet":True},
}

# Used with ScreenManager
class Screen1(Screen):
    pass

class Screen2(Screen):
    pass

class Screen3(Screen):
    pass

class Screen4(Screen):
    pass

class Screen5(Screen):
    pass

class Error_popup(RelativeLayout):
    pass

class Choose_coin_popup(RelativeLayout):
    pass


class SaveDialog(FloatLayout):
    """The file save dialog class
    """
    save_to_storage = ObjectProperty(None)
    text_input = ObjectProperty(None)
    cancel = ObjectProperty(None)

class MyManager(ScreenManager):
    """The screen manager that handles app screens
    """
    pass

class MainBox(BoxLayout):
    """Mainbox under MainApp
    It contains the ScreenManager
    """
    pass

# gettext support based on https://github.com/tito/kivy-gettext-example
class Lang(Observable):
    """
    Support gettext in Kivy based on `tito` example.
    """
    observers = []
    lang = None

    def __init__(self, defaultlang):
        super(Lang, self).__init__()
        self.ugettext = None
        self.lang = defaultlang
        self.switch_lang(self.lang)

    def _(self, text):
        return self.ugettext(text)

    def fbind(self, name, func, args, **kwargs):
        if name == "_":
            self.observers.append((func, args, kwargs))
        else:
            return super(Lang, self).fbind(name, func, *args, **kwargs)

    def funbind(self, name, func, args, **kwargs):
        if name == "_":
            key = (func, args, kwargs)
            if key in self.observers:
                self.observers.remove(key)
        else:
            return super(Lang, self).funbind(name, func, *args, **kwargs)

    def switch_lang(self, lang):
        # get the right locales directory, and instanciate a gettext
        locale_dir = join(dirname(__file__), 'data', 'locales')
        locales = gettext.translation(app_name, locale_dir, languages=[lang])
        self.ugettext = locales.gettext
        self.lang = lang

        # update all the kv rules attached to this text
        for func, largs, kwargs in self.observers:
            func(largs, None, None)

# Set the language of App
tr = Lang(settings["app_language"])


class ArLabel(TextInput):
    """
    Labels with Arabic support
    """
    str = StringProperty()

    def __init__(self, **kwargs):
        super(ArLabel, self).__init__(**kwargs)
        self.text = get_display(arabic_reshaper.reshape(tr._('filled 2')))

class MainApp(App):
    """
    Main App class
    """
    lang = StringProperty(settings["app_language"])
    
    def on_lang(self, instance, lang):
        tr.switch_lang(lang)

    def ar_shape(self, text):
        """
        Convert Arabic text for non compatible display
        """
        reshaped_text = arabic_reshaper.reshape(text)
        return get_display(reshaped_text)


    def save_settings(self):
        """
        Save settings from configration screen to the database.
        """
        self.update_settings = Settings.update(
                                            balance_api= self.root.ids.screen5.ids.balance_api.text,
                                            token_id = self.root.ids.screen5.ids.token_id.text,
                                            coin_prefix = self.root.ids.screen5.ids.coin_prefix.text,
                                            ).where(Settings._id == 1)
        self.update_settings.execute()
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        Logger.debug(f"save_settings: Current coin prefix {self.root.ids.screen5.ids.coin_prefix.text}")
        Logger.debug(f'save_settings: Current wallet record {wallet_records.address.split(":")[0]}')

        if self.root.ids.screen5.ids.coin_prefix.text !=wallet_records.address.split(":")[0]:
            ["prefix_changed_recently"] == True
            error_text = tr._(" Changes saved! \n Please regenerate the wallet\n to start using the new prefix")
            self.show_error_popup(error_text)
            Logger.debug("save_settings: Data saved. Prefix mismatch.")
        else:
            Logger.debug("save_settings: Data saved. Prefix not altered.")
    
    
    def create_wallet(self, prefix):
        """Create wallet using bitcoinaddress lib then convert it to
        SLP address using cashaddress lib
        """
        self.app_settings = Settings.get(Settings._id == 1)
        # Create wallet address using ecashaddress lib and set custom prefix
        Logger.debug(f"in wallet generation {prefix}")

        if supported_coins[self.app_settings.coin_prefix]["testnet"] == False:
            wallet = Wallet(testnet=False)
            MainApp.wif = wallet.key.mainnet.wifc
            #If currency not bitcoin use pubaddr1c
            if self.app_settings.coin_prefix != "bitcoin":
                MainApp.address = Address.from_string(
                            wallet.address.mainnet.pubaddr1c
                            ).to_cash_address(prefix=prefix)
            #If currency bitcoin use pubaddrbc1_P2WPKH for newer address format
            else:
                MainApp.address = wallet.address.mainnet.pubaddrbc1_P2WPKH
        else:
            wallet = Wallet(testnet=True)
            MainApp.wif = wallet.key.testnet.wifc
            if self.app_settings.coin_prefix == "bitcoin":
                MainApp.address = wallet.address.testnet.pubaddrbc1_P2WPKH
            else:
                MainApp.address = Address.from_string(
                            wallet.address.testnet.pubaddr1c
                            ).to_cash_address(prefix=prefix)
        
        settings["prefix_changed_recently"] = False
        # Logging wallet address
        Logger.info(f"create_wallet: new wallet address {MainApp.address} .")
        Logger.debug(f"create_wallet: new wallet WIF {MainApp.wif} .")

        # Saving new wallet to database
        db_save_wallets(MainApp.address,  MainApp.wif)

        # Logging to debug
        Logger.debug(f"create_wallet:New address: {MainApp.address} and {MainApp.wif}")
        Logger.debug(f"create_wallet: New Wif: {MainApp.wif}")

    def contact_server(self):
        self.root.ids.screen1.ids.progress_bar.value = 1
        if self.app_settings.coin_prefix == "simpleledger":
            # Convert SLP addrress to bitcoin cash for API requirement
            bch_address = Address.from_string(MainApp.address
                        ).to_cash_address(prefix='bitcoincash')
            Logger.debug(f"get_balance: BCH address to query {bch_address}")
            address_data = { "address": bch_address }
            token_data = { "tokenId": self.app_settings.token_id }
            self.root.ids.screen1.ids.progress_bar.value = 2
            try:
                # get token data on address
                response = requests.post(self.app_settings.balance_api+"address/", json=address_data)
                self.root.ids.screen1.ids.progress_bar.value = 5
                Logger.debug(f"get_balance: Got data about the address from {self.app_settings.balance_api}")
                # get token decimals from token info
                response2 = requests.post(self.app_settings.balance_api+"token/data", json=token_data)
                self.root.ids.screen1.ids.progress_bar.value = 7
                Logger.debug(f"get_balance: Got data about the token")
                balance_data = response.json()
                token_data = response2.json()
                token_decimals = token_data["genesisData"]["decimals"]
                # Check if address is availbe in API
                if response.status_code == 200 :
                    try:
                        tokens_list = balance_data["balance"]["balances"]
                        # descending into a list of token balances
                        _data  = next((d for d in tokens_list if d['tokenId'] == self.app_settings.token_id), None)
                        token_balance = int(_data['qty']) / 10**token_decimals
                        self.root.ids.screen1.ids.balance_field.text = tr._("Balance: ")+str(token_balance)
                        self.root.ids.screen1.ids.progress_bar.value = 10
                        Logger.info(f"get_balance: Balance from API: {token_balance}")
                    except Exception as e:
                        error_text = tr._("No Balance found for token.")
                        # ~ MainApp.show_error_popup(self, error_text)
                        Logger.debug(f"get_balance: No Balance found for token.{e} ")
                else:
                    self.root.ids.screen1.ids.progress_bar.value = 10
                    self.root.ids.screen1.ids.balance_field.text = tr._("Balance: ")+"?"
                    error_text = tr._("Address empty?")
                    # ~ MainApp.show_error_popup(self, error_text)
                    Logger.debug(f"get_balance: status_code: {response.status_code}")
            except Exception as e:
                error_text = tr._("There is an error connecting to API.")
                # ~ MainApp.show_error_popup(self, error_text)
                Logger.debug(f"get_balance: There is an error connecting to API.{e} ")
        else:
            error_text = tr._("Coin not supported.")

    def get_balance(self):
        """
        Get balance using an external source
        """
        try:
            threading.Thread(target=self.contact_server).start()
        except:
            Logger.info("test noadsofoadsofa")

            # ~ MainApp.show_error_popup(self, error_text)


    def generate_qr_code(self):
        """Generate QR code for address and WIF
        """

        if not os.path.exists(media_dir):
            # if the media directory is not present
            # then create it.
            os.makedirs(media_dir)

        # Generating QRcode for wallet address
        qr = pyqrcode.create(MainApp.address, mode='binary')
        
        # Saving QRcode to png file using pypng 
        qr.png(address_qrcode_file, scale=15)
        
        # Logging the operation to loglevel debug
        Logger.debug(f"generate_qr_code: file {address_qrcode_file} created.")

        # Generating QRcode for WIF
        qr2 = pyqrcode.create(MainApp.wif, mode='binary')
        
        # Saving QRcode to png file using pypng 
        qr2.png(wif_qrcode_file, scale=15)
        
        # Logging the operation to log level debug
        Logger.debug(f"generate_qr_code: file {wif_qrcode_file} created.")
        

    def manage_keys(self):
        """Control screen to manage keys and show active one
        """
        
        # Clear the current label data
        # ~ self.root.ids.sm.container.clear_widgets()
        
        # Fetch 
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        self.root.ids.sm.container.text = str(wallet_records._id)+", "+wallet_records.address
        Logger.debug(f'manage_keys: Number of records: {wallet_records._id}')

        self.root.ids.sm.current = 'Keys'

    def prefix_match(self):
        self.app_settings = Settings.get(Settings._id == 1)
        wallet_records = Wallets.select().order_by(Wallets._id.desc()).get()
        record_prefix = wallet_records.address.split(":")[0]
        Logger.debug(f"prefix_match: Record_prefix {record_prefix}")
        Logger.debug(f"prefix_match: app settings {self.app_settings.coin_prefix}")

        if self.app_settings.coin_prefix != "bitcoin":
            if record_prefix == self.app_settings.coin_prefix:
                return True
            else:
                return False
        # Check if address is not cache address and it's bitcoin address with bc
        elif ( ":" not in wallet_records.address
            and wallet_records.address.startswith("bc")
            and (self.app_settings.coin_prefix == "bitcoin")
            ):
            Logger.debug(f"prefix_match: prefix:{self.app_settings.coin_prefix}")
            Logger.debug(f'prefix_match: wallet starts with bc: {wallet_records.address.startswith("bc")}')
            return True
        else:
            return False


    def view_address(self):
        """Address QRcode screen
        """
        # Set source of address qrcode image
        self.root.address_qrcode.source = address_qrcode_file
        
        Logger.info(f"Address shown: {MainApp.address}")
        # Force reload of image to fix issue not reading file
        self.root.address_qrcode.reload()
        
        # Wait for some time before continuing
        time.sleep(0.5)

        # Assign labels

        if self.prefix_match():
            self.root.ids.sm.address_label.text = MainApp.address
        else:
            self.root.ids.sm.address_label.text = tr._("Regenerate wallet!")
            Logger.info(f"view_address: Prefix mismatch")

        # Switch to screen0
        self.root.ids.sm.current = 'Address'
        
    def copy_address(self, dt):
        Clipboard.copy(self.root.ids.sm.address_label.text)
        self.root.ids.screen2.ids.copy_message.text = tr._("Copied!")
        Logger.debug("address copied to clipboard")

    def copy_wif(self, dt):
        Clipboard.copy(self.root.ids.sm.wif_label.text)
        self.root.ids.screen3.ids.copy_message.text = tr._("Copied!")
        Logger.debug("wif copied to clipboard")

    def view_wif(self):
        """WIF QRcode screen
        """
        # Set source of WIF QRcode image
        self.root.wif_qrcode.source = wif_qrcode_file
        
        # Force reload of image to fix issue not reading file
        self.root.wif_qrcode.reload()
        
        # Wait for some time before continuing
        time.sleep(0.5)
        
        # Assign labels
        Logger.debug(f"Wif shown {MainApp.address}")
        self.root.ids.sm.wif_label.text = f'{MainApp.wif}'
        
        # Switch to screen
        self.root.ids.sm.current = 'WIF'

    def show_settings(self):
        """Move to settings screen
        """
        error_text = tr._(" Please be careful\n changing settings may\n break the app functionality")
        self.show_error_popup(error_text)
        MainApp.app_settings = Settings.get(Settings._id == 1)
        self.root.ids.screen5.ids.coin_prefix.text = MainApp.app_settings.coin_prefix
        self.root.ids.screen5.ids.balance_api.text = MainApp.app_settings.balance_api
        self.root.ids.screen5.ids.token_id.text = MainApp.app_settings.token_id
        self.root.ids.sm.current = 'Screen5'


    def first_start(self):
        """
        Checks to be done at first start of the App
        """
        
        # Create setting database if not exist
        if Settings.select().exists():
            self.app_settings = Settings.get(Settings._id == 1)
            Logger.debug(f"first_start: Got existing settings.")
        else:
            db_save_settings(
                            balance_api=default_balance_api,
                            token_id=default_token_id,
                            coin_prefix=default_coin_prefix,
                            )
            self.app_settings = Settings.get(Settings._id == 1)
            Logger.debug(f"first_start: Created settings from defaults.")
            
        # If no entry in database generate wallet and create QRcode
        if Wallets.select().exists() == False:
            self.app_settings = Settings.get(Settings._id == 1)
            self.create_wallet(self.app_settings.coin_prefix)
            self.generate_qr_code()
            Logger.info(
                f"no entry was found so new wallet and QR images were created."
                )
        else:
            # set the wallet address and wif
            wallet = Wallets.select().order_by(Wallets._id.desc()).get()
            MainApp.address = wallet.address
            MainApp.wif = wallet.wif

            # Log it
            Logger.debug(f"first_start: Values used from an existing wallet.")
            Logger.debug(f"first_start: Address {MainApp.address}.")
            Logger.debug(f"first_start: WIF {MainApp.wif}.")


    def show_popup(self):
        """
        Shows confirmation popup before generating a new wallet and 
        replacing the QRcodes with new ones.
        """
        
        # Create a new instance of the Confirm_popup class 
        show = Confirm_popup() 

        # Setup the popup window
        popupWindow = Popup(title=tr._("Confirm Generating"),
                            content=show,
                            # ~ size_hint=(None,None),
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open() # show the popup

    def show_error_popup(self,error_text):
        """
        Shows error popup
        """
        # Create a new instance of the Confirm_popup class
        show = Error_popup()
        error_label = Label(text=error_text)
        show.add_widget(error_label)
        # Setup the popup window
        popupWindow = Popup(title=tr._("Error"),
                            content=show,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        popupWindow.open() # show the popup

    def show_choose_coin_popup(self):
        """
        Shows choose coin popup
        """
        # Create a new instance of the Confirm_popup class
        show = Choose_coin_popup()
        box = BoxLayout(orientation="vertical")
        for i in supported_coins:
            toggle_button = ToggleButton(text=str(i), group="coins")
            # Using `partial` magic
            toggle_button.bind(on_release=partial(self.select_coin, toggle_button, i))
            box.add_widget(toggle_button)
        show.add_widget(box)
        # Setup the popup window
        self.popupWindow = Popup(title=tr._("Choose a Coin"),
                            content=show,
                            size_hint=(.9, .6),
                            auto_dismiss=True
                            )

        # Create the popup window
        self.popupWindow.open() # show the popup

    def select_coin(self, toggle_button, choice, btn):
        self.root.ids.screen5.ids.coin_prefix.text = str(choice)
        self.popupWindow.dismiss()


    def dismiss_popup(self):
        self._popup.dismiss()

    ###### section for file chooser #######
    def show_export_window(self):
        """
        Show the export to file dialog
        """
        # Request permission
        if platform == 'android':
            from android.permissions import request_permissions, Permission
            request_permissions([Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE])
            Logger.info('show_export_window: Platform Android.')
        else:
            Logger.info('show_export_window: Platform not Android.')
        self.export_keys()
        content = SaveDialog(save_to_storage=self.save_to_storage, cancel=self.dismiss_popup)
        self._popup = Popup(title=tr._("Save file"), content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()


    def save_to_storage(self, path, filename):
        """
        Write json formatted wallet keys to storage
        """
        with open(os.path.join(path, filename), 'w') as stream:
            stream.write(self.key_list)

        self.dismiss_popup()


##### end section for file chooser ######

    def export_keys(self):
        """
        Export wallet keys from database to json format
        """
        with sqlite3.connect(database_file) as db:
            cursor = db.cursor()
            cursor.execute('''SELECT * from wallets ''')
            result=cursor.fetchall()
            self.key_list = json.dumps(result, sort_keys=True, indent=4)

        
    # ~ def show_notification(self):
        # ~ plyer.notification.notify(
            # ~ title='QR Code generator', message="Qr code created")
    def build(self):
        self.icon = "data/icon.png"
        Builder.load_file(layout_file)
        return MainBox()

    def on_start(self, **kwargs):
        self.first_start()

class Confirm_popup(RelativeLayout):
    def validate_password(self):
        """
        Generate a new wallet and QRcode images if password is correct
        """
        if self.ids.password.text == settings["confirm_delete_text"]:
            MainApp.app_settings = Settings.get(Settings._id == 1)
            MainApp.create_wallet(self, MainApp.app_settings.coin_prefix)
            MainApp.generate_qr_code(self)
            self.popup_text.text = tr._("Done!")
            self.delete_button.disabled = True
            self.popup_text.text = tr._("New wallet generated!")
            
        else:
            self.ids.popup_text.text = tr._("Wrong Password!")
    pass



if __name__ == '__main__':
    MainApp().run()
